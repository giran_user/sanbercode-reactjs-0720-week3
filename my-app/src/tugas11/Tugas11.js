import React, { Component } from 'react';
import './Tugas11.css'

class Tugas11 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                {id:1, nama: "Semangka", harga: 10000, berat: 1000},
                {id:2, nama: "Anggur", harga: 40000, berat: 500},
                {id:3, nama: "Strawberry", harga: 30000, berat: 400},
                {id:4, nama: "Jeruk", harga: 30000, berat: 1000},
                {id:5, nama: "Mangga", harga: 30000, berat: 500}
            ]

        }
    }

    renderTableData() {
        return this.state.dataHargaBuah.map((buah,index) => {
            const {id, nama, harga, berat} = buah
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{nama}</td>
                    <td>{harga}</td>
                    <td>{berat/1000} kg</td>
                </tr>
            )

        })
    }

    renderTableHeader() {
        let header = Object.keys(this.state.dataHargaBuah[0])
        return header.map((key, index) => {
            return <th key={index}>{key.charAt(0).toUpperCase() + key.slice(1)}</th>
        })
    }



    render() {
        return (
            <div>
                <h1 id='title'>Tabel Harga Buah</h1>
                <table id='daftarHargaBuah'>
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                    </tbody>
                </table>                
            </div>
        );
    }
}

export default Tugas11;