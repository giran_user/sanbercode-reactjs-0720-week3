import React, {useState, useEffect} from 'react'
import axios from "axios";
import './Tugas14.css'

function Tugas14() {
    const [input, setInput] = useState({
        name: "",
		price: "",
		weight: ""
	})
    const [dataHargaBuah, setDataHargaBuah] = useState(null)
	const [statusForm, setStatusForm] = useState("create")
    const [selectId, setSelectedId] = useState(0)
    
    useEffect(() =>{
		if(dataHargaBuah === null){
			axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
				.then(res => {
					setDataHargaBuah(res.data.map(el => {return {id:el.id, name:el.name, price:el.price, weight:el.weight }}))
				})
		}
    },[dataHargaBuah])   
    
    function handleEdit(event) {
        let id = Number(event.target.value)
		let buah = dataHargaBuah.find(x => x.id === id)
		setInput({name: buah.name, price: buah.price, weight: buah.weight})
		setSelectedId(id)
		setStatusForm("edit")
    }
    
    function handleDelete(event) {
        let id = Number(event.target.value)
		let newDataBuah = dataHargaBuah.filter(el => el.id !== id)

		axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
			.then(res => {
				console.log(res)
			})
		setDataHargaBuah([...newDataBuah])
        
    }    
    
    function handleChange(event) {
        const { name, value } = event.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
		}))
    }

    function handleSubmit(event) {
        event.preventDefault()
		if(input['name'] !== "" && input['price'].toString() !== "" && input['weight'].toString() !== "" ){
			if(statusForm === "create"){
				axios.post(`http://backendexample.sanbercloud.com/api/fruits`, input)
					.then(res => {
						console.log(res.data)
						setDataHargaBuah([...dataHargaBuah, {name: res.data.name, price: res.data.price, weight: res.data.weight}])
					})

				
			}else if(statusForm === "edit"){
				axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectId}`, input)
					.then(res => {
						let buah = dataHargaBuah.find(el => el.id === selectId)
						buah['name'] = input.name
						buah['price'] = input.price
						buah['weight'] = input.weight
						setDataHargaBuah([...dataHargaBuah])
					})
			}

			setStatusForm("create")
			setSelectedId(0)
			setInput({
				name: "",
				price: "",
				weight: ""
			})
		}
    }

    return (
        <>
		<h1>Tabel Harga Buah</h1>
		<table>
			<tr>
            <th>{"No"}</th>
            <th>{"Nama"}</th>
            <th>{"Harga"}</th>
            <th>{"Berat"}</th>
            <th>{"Aksi"}</th>
			
		  </tr>
			{dataHargaBuah !== null && dataHargaBuah.map((el,index) =>{
				return(
					<tr key={index}>
                        <td>{index+1}</td>
                        <td>{el.name}</td>
                        <td>{el.price}</td>
                        <td>{el.weight/1000 + "kg"}</td>							
						
						<td style={{textAlign:'center'}}>
							<button onClick={handleEdit} value={el.id}>Edit</button>
							&nbsp;
							<button onClick={handleDelete} value ={el.id}>Delete</button>
						</td>
					</tr>
				)
			})}
		</table>
		<h1>Form Harga Buah</h1>
		<div id="content-form">
		<form onSubmit={handleSubmit}>
			<label> Nama : </label>          
			<input type="text" name='name' value={input.name} onChange={handleChange} />
            <br/><br/>
			<label>Harga : </label>
			<input type="text" name='price' value ={input.price} onChange={handleChange} />
            <br/><br/>
			<label>Berat : </label>
			<input type="text" name='weight' value={input.weight} onChange={handleChange} />
            <br/><br/>
			<button style={{borderRadius:"10px"}} >Save</button>
		</form>
		</div>
		</>
    )
}

export default Tugas14
