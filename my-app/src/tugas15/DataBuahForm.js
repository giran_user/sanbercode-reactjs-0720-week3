import React, {useContext} from 'react'
import axios from "axios"
import { DataBuahContext } from "./DataBuahContext"

function DataBuahForm() {
    const [dataBuah, setDataBuah, idBuah, setIdBuah, input, setInput, status, setStatus] = useContext(DataBuahContext)

    function handleSubmit(event) {
        event.preventDefault()
		if(input['name'] !== "" && input['price'].toString() !== "" && input['weight'].toString() !== "" ){
			if(status === "create"){
				axios.post(`http://backendexample.sanbercloud.com/api/fruits`, input)
					.then(res => {
						console.log(res.data)
						setDataBuah([...dataBuah, {name: res.data.name, price: res.data.price, weight: res.data.weight}])
					})
			}else if(status === "edit"){
				axios.put(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`, input)
					.then(res => {
						let buah = dataBuah.find(el => el.id === idBuah)
						buah['name'] = input.name
						buah['price'] = input.price
						buah['weight'] = input.weight
						setDataBuah([...dataBuah])
					})
			}

			setIdBuah(0)
			setInput({
				name: "",
				price: "",
                weight: ""
            })
            setStatus("create")
		}
    }

    const handleChange = (event) => {
        const { name, value } = event.target;
            setInput(prevState => ({
                ...prevState,
                [name]: value
            }))
    }


    return (
        <>
            <h1 style={{textAlign:"center", marginTop: '25px', marginBottom: '25px'}}>Form Tabel Buah</h1>
            <form onSubmit={handleSubmit} style={{width:'25%', marginLeft: 'auto', marginRight: 'auto'}}>
                <div>
                    <label> Nama : </label>          
                    <input type="text" name='name' value={input.name} onChange={handleChange} />
                </div>
                <div>
                    <label>Harga : </label>
                    <input type="text" name='price' value ={input.price} onChange={handleChange} />
                </div>
                <div>
                    <label>Berat : </label>
                    <input type="text" name='weight' value={input.weight} onChange={handleChange} />
                </div>
                <br/>
                <button type="submit">save</button>
            </form>
            
        </>
    )
}

export default DataBuahForm
