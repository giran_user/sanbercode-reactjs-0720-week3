import React, { useState, createContext } from "react";

export const DataBuahContext = createContext();

export const BuahProvider = props => {
  const [dataBuah, setDataBuah] = useState(null);
  const [idBuah, setIdBuah] = useState(0);
  const [input, setInput] = useState({
		name: "",
		price: "",
        weight: "",
  })
  const [status, setStatus] = useState("create")
  
  return (
    <DataBuahContext.Provider value={[dataBuah, setDataBuah, idBuah, setIdBuah, input, setInput, status, setStatus]}>
      {props.children}
    </DataBuahContext.Provider>
  );
};