import React, { useContext, useEffect } from "react"
import axios from 'axios';
import { DataBuahContext } from "./DataBuahContext"
import DataBuahAction from './DataBuahAction'

function DataBuahList() {
    const [dataBuah, setDataBuah] = useContext(DataBuahContext)

    useEffect(() =>{
		if(dataBuah === null){
			axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
				.then(res => {
					setDataBuah(res.data.map(el => {return {id:el.id, name:el.name, price:el.price, weight:el.weight }}))
				})
		}
    })

    return (
        <>
            <h1 style={{textAlign:"center", marginTop: '25px', marginBottom: '25px'}}>Tabel Harga Buah</h1>
	  	    <table class="table table-striped" style={{width:'75%', marginLeft: 'auto', marginRight: 'auto'}}>
                <thead class = "thead-dark">
                    <tr>
                        <th> No </th>
                        <th> Nama </th>
                        <th> Harga </th>
                        <th> Berat </th>
                        <th> Aksi </th>
                    </tr>
                </thead>
                {dataBuah !== null && dataBuah.map((el,index) =>{
                    return(
                        <tr key={index}>
                            <td> {index + 1}</td>
                            <td> {el.name}</td>
                            <td> {el.price}</td>
                            <td> {el.weight/1000 + 'kg'}</td>
                            <DataBuahAction id = {el.id}/>
                        </tr>
                    )
                })}
            </table>            
        </>
    )
}

export default DataBuahList
