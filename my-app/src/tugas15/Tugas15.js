import React from "react"
import { BuahProvider } from "./DataBuahContext"
import DataBuahList from "./DataBuahList"
import DataBuahForm from "./DataBuahForm"

const Buah = () =>{
  return(
    <BuahProvider>
      <DataBuahList/>
      <DataBuahForm/>
    </BuahProvider>
  )
}

export default Buah;