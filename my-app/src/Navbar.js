import React from 'react'
import { Link } from "react-router-dom";
import './Navbar.css'

function Navbar() {
    return (
        <>
            <div id="container">
                <div id="navbar">                    
                    <div class="toggle">                        
                        <i class="fa fa-bars menu" aria-hidden="true"></i>
                    </div>
                    
                    <div id="menu">
                        <ul>
                            <li><a href=""><Link to="/">Tugas 11</Link></a></li>
                            <li><a href=""><Link to="/Tugas12">Tugas 12</Link></a></li>
                            <li><a href=""><Link to="/Tugas13">Tugas 13</Link></a></li>
                            <li><a href=""><Link to="/Tugas14">Tugas 14</Link></a></li>
                            <li><a href=""><Link to="/Tugas15">Tugas 15</Link></a></li>
                        </ul>
                    </div>

                </div>
            </div>            
        </>
    )
}

export default Navbar
