import React from 'react'
import { Switch, Route } from "react-router";
import Tugas11 from './tugas11/Tugas11';
import Tugas12 from './tugas12/Tugas12';
import Tugas13 from './tugas13/Tugas13';
import Tugas14 from './tugas14/Tugas14';
import Tugas15 from './tugas15/Tugas15';

function Router() {
    return (
        <Switch>
            <Route exact path="/" >
                <Tugas11 />
            </Route>
            <Route exact path="/Tugas12" >
                <Tugas12 />
            </Route>
            <Route exact path="/Tugas13" >
                <Tugas13 />
            </Route>
            <Route exact path="/Tugas14" >
                <Tugas14 />
            </Route>
            <Route exact path="/tugas15" >
                <Tugas15 />
            </Route>
        </Switch>
    )
}

export default Router
