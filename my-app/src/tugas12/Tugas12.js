import React, { Component } from "react";

class Tugas12 extends Component{
    constructor(props){
      super(props)
      this.state = {
        time: 100,
        jam: new Date().toLocaleTimeString().slice(0,10).split("/")

      }
    }

    componentDidMount(){
        if (this.props.start !== undefined){
          this.setState({time: this.props.start})
        }
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
    }
    
    componentDidUpdate() {        
        if(this.state.time < 1){
            this.componentWillUnmount()
        }
    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }
    
    tick() {
        this.setState({
          time: this.state.time - 1 
        });
    }


    render(){
        return(
          <>
          {this.state.time >= 1 && 
          
            <h1 style={{textAlign: "center"}}>
             sekarang jam : {this.state.jam}
             <span style={{marginLeft:"60px"}}>
                hitung mundur {this.state.time}
             </span>
            </h1>
          
          }
            {/* <h1 style={{textAlign: "center"}}>
              Hitung Mundur {this.state.time}
            </h1> */}
          </>
        )
    }
}

export default Tugas12